class { 'apache':
  default_vhost => false,
  mpm_module => 'prefork'
}

# Make sure mod_rewrite and mod_remoteip are installed
apache::mod { 'rewrite': }
apache::mod { 'remoteip': }
apache::mod { 'proxy': }
apache::mod { 'proxy_http': }

include apache::mod::php

# Define the SSL options
$ssl_options = '/etc/letsencrypt/options-ssl-apache.conf'

# Define SSL certificate and key for telepedia.net
$ssl_cert_telepedia = '/etc/letsencrypt/live/telepedia.net/fullchain.pem'
$ssl_key_telepedia = '/etc/letsencrypt/live/telepedia.net/privkey.pem'

# Define SSL certificate and key for whiki.online
$ssl_cert_whiki = '/etc/letsencrypt/live/whiki.online/fullchain.pem'
$ssl_key_whiki = '/etc/letsencrypt/live/whiki.online/privkey.pem'

# Redirect HTTP to HTTPS
apache::vhost { 'mediawiki.telepedia.net:80':
  servername      => 'mediawiki.telepedia.net',
  port            => 80,
  docroot         => '/var/www/html/mediawiki1',
  serveraliases   => ['*.telepedia.net'],
  error_log_file  => 'error.log',
  access_log_file => 'access.log',
  custom_fragment => 'RewriteEngine On
                      RewriteCond %{HTTPS} off
                      RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]',
}

apache::vhost { 'grafana.telepedia.net:80':
  servername      => 'grafana.telepedia.net',
  port            => 80,
  docroot         => '/var/www/html',  # or a different directory if you prefer
  serveraliases   => ['grafana.telepedia.net'],
  error_log_file  => 'error.log',
  access_log_file => 'access.log',
  custom_fragment => 'RewriteEngine On
                      RewriteCond %{HTTPS} off
                      RewriteRule (.*) https://%{HTTP_HOST}%{REQUEST_URI} [R=301,L]',
}

# Define the SSL virtual hosts
apache::vhost { 'phpmyadmin':
  port        => 443, # Assuming SSL, change to your port if different
  docroot     => '/var/www/html',
  ssl         => true,
  ssl_cert    => $ssl_cert_telepedia,
  ssl_key     => $ssl_key_telepedia,
  aliases     => [
    {
      alias => '/phpmyadmin',
      path  => '/usr/share/phpMyAdmin'
    }, {
      alias => '/phpMyAdmin',
      path  => '/usr/share/phpMyAdmin'
    }
  ],
  directories => [
    {
      'path'  => '/usr/share/phpMyAdmin/',
      'allow' => 'from all',
    }, {
      'path'  => '/usr/share/phpMyAdmin/setup/',
      'deny'  => 'from all',
    }, {
      'path'  => '/usr/share/phpMyAdmin/libraries/',
      'deny'  => 'from all',
    }
  ],
}

apache::vhost { 'phorge.telepedia.net':
  port            => 443,
  docroot         => '/var/www/html/phorge/webroot',
  servername      => 'phorge.telepedia.net',
  serveraliases   => ['phorge.telepedia.net'],
  error_log_file  => 'error.log',
  access_log_file => 'access.log',
  ssl             => true,
  priority        => 10,
  ssl_cert        => $ssl_cert_telepedia,
  ssl_key         => $ssl_key_telepedia,
  custom_fragment => 'RewriteEngine on
                      RewriteRule ^(.*)$          /index.php?__path__=$1  [B,L,QSA]',
}


apache::vhost { 'gamecheats.wiki':
  port            => 443,
  docroot         => '/var/www/html/mediawiki1',
  serveraliases   => ['gamecheats.wiki'],
  error_log_file  => 'error.log',
  access_log_file => 'access.log',
  ssl             => true,
  ssl_cert        => $ssl_cert_telepedia,
  ssl_key         => $ssl_key_telepedia,
  custom_fragment => 'RemoteIPHeader CF-Connecting-IP',
}

apache::vhost { 'telepedia.net':
  port            => 443,
  docroot         => '/var/www/html/wordpress',
  serveraliases   => ['telepedia.net'],
  error_log_file  => 'error.log',
  access_log_file => 'access.log',
  ssl             => true,
  options         => ['FollowSymLinks', 'MultiViews'],
  override        => ['All'],
  ssl_cert        => $ssl_cert_telepedia,
  ssl_key         => $ssl_key_telepedia,
  custom_fragment => "RemoteIPHeader CF-Connecting-IP\nInclude ${ssl_options}",
}

apache::vhost { 'staging.telepedia.net':
  port            => 443,
  docroot         => '/var/www/html/mediawiki2',
  serveraliases   => ['testingoa.telepedia.net'],
  directories     => [
    {
      'path'            => '/var/www/html/mediawiki2',
      'options'         => ['FollowSymLinks','MultiViews'],
      'allow_override'  => ['None'],
      'require'         => 'all granted'
    }
  ],
  error_log_file  => 'error.log',
  access_log_file => 'access.log',
  options         => ['FollowSymLinks', 'MultiViews'],
  ssl             => true,
  ssl_cert        => '/etc/letsencrypt/live/telepedia.net/fullchain.pem',
  ssl_key         => '/etc/letsencrypt/live/telepedia.net/privkey.pem',
  custom_fragment => "
    LogLevel alert rewrite:trace6
    RemoteIPHeader CF-Connecting-IP
    Include /etc/letsencrypt/options-ssl-apache.conf
    RewriteEngine On
    RewriteRule ^/?wiki(/.*)?$ %{DOCUMENT_ROOT}/index.php [L]
    RewriteRule ^/?$ %{DOCUMENT_ROOT}/index.php [L]
    RewriteRule ^/sitemap\\.xml$ /sitemap.php [NS]
    RewriteRule ^/extensions/ - [R=403,L]
    RewriteRule ^/ads.txt https://config.playwire.com/dyn_ads/1025055/74539/ads.txt [R=301]
    ErrorDocument 404 /404.php
    Alias '/robots.txt' '/etc/puppetlabs/code/environments/production/modules/apache/files/robots.php'
  ",
}

apache::vhost { 'mediawiki.telepedia.net':
  port            => 443,
  docroot         => '/var/www/html/mediawiki1',
  serveraliases   => ['*.telepedia.net'],
  directories     => [
    {
      'path'            => '/var/www/html/mediawiki1',
      'options'         => ['FollowSymLinks','MultiViews'],
      'allow_override'  => ['None'],
      'require'         => 'all granted'
    }
  ],
  error_log_file  => 'error.log',
  access_log_file => 'access.log',
  options         => ['FollowSymLinks', 'MultiViews'],
  ssl             => true,
  ssl_cert        => '/etc/letsencrypt/live/telepedia.net/fullchain.pem',
  ssl_key         => '/etc/letsencrypt/live/telepedia.net/privkey.pem',
  custom_fragment => "
    LogLevel alert rewrite:trace6
    RemoteIPHeader CF-Connecting-IP
    Include /etc/letsencrypt/options-ssl-apache.conf
    RewriteEngine On
    RewriteRule ^/?wiki(/.*)?$ %{DOCUMENT_ROOT}/index.php [L]
    RewriteRule ^/?$ %{DOCUMENT_ROOT}/index.php [L]
    RewriteRule ^/sitemap\\.xml$ /sitemap.php [NS]
    RewriteRule ^/extensions/ - [R=403,L]
    RewriteRule ^/ads.txt https://config.playwire.com/dyn_ads/1025055/74539/ads.txt [R=301]
    ErrorDocument 404 /404.php
    Alias '/robots.txt' '/etc/puppetlabs/code/environments/production/modules/apache/files/robots.php'
  ",
}


apache::vhost { 'grafana.telepedia.net':
  port            => 443,
  docroot         => '/var/www/html', # Add this line
  servername      => 'grafana.telepedia.net',
  serveraliases   => ['grafana.telepedia.net'],
  error_log_file  => 'error.log',
  access_log_file => 'access.log',
  ssl             => true,
  ssl_cert        => $ssl_cert_telepedia,
  ssl_key         => $ssl_key_telepedia,
  custom_fragment => "RemoteIPHeader CF-Connecting-IP\nInclude ${ssl_options}\nProxyPreserveHost On\nProxyPass / http://5.75.199.177:3000/\nProxyPassReverse / http://5.75.199.177:3000/",
}
