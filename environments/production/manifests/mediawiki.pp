class mediawiki {

  package { 'git':
    ensure => installed,
  }

  vcsrepo { '/var/www/html/mediawiki1':
    ensure   => latest,
    provider => git,
    source   => 'https://gitlab.com/telepedia/MediaWiki',
    require  => Package['git'],
  }
}

include mediawiki
