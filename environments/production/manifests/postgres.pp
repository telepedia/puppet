include postgresql::globals

class { 'postgresql::server':
  version => 'latest',
  postgres_password => file('/etc/private/postgres_password.txt')
}

postgresql::server::db { 'discourse':
  user     => 'discourseAdmin',
  password => postgresql::postgresql_password('discourse', 'telepediaOA109'),
}